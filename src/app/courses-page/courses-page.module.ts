import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesPageComponent } from './courses-page.component';
import { CoursesListModule } from './courses-list/courses-list.module';
import { ToolboxComponent } from './toolbox/toolbox.component';
import { SharedModule } from '../shared/shared.module';
import { CoursesRoutingModule } from './courses-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    CoursesListModule,
    SharedModule,
  ],
  declarations: [
    CoursesPageComponent,
    ToolboxComponent,
  ],
})
export class CoursesPageModule { }
