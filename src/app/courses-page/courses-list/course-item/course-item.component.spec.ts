import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CourseItemComponent } from './course-item.component';
import { IconComponent } from '../../../shared/icon/icon.component';
import { AlternateBorderDirective } from '../../../shared/directives/alternateborder.directive';
import { DurationPipe } from '../../../shared/pipes/duration/duration.pipe';

export const COURSE_MOCK = {
  'id': 1,
  'title': 'Course Number 1',
  'date': 'June, 26 2017',
  'duration': 245,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
};

describe('CourseItemComponent', () => {
  let component: CourseItemComponent;
  let fixture: ComponentFixture<CourseItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternateBorderDirective, IconComponent, DurationPipe, CourseItemComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemComponent);
    component = fixture.componentInstance;
    component.courseItem = COURSE_MOCK;
    fixture.detectChanges();
    spyOn(component, 'onDeleteClick');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have course data', () => {
    expect(component.courseItem).toEqual(COURSE_MOCK);
  });

  it('should have course title', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const span: HTMLElement = nativeElement.querySelector('.course-title');
    const TITLE = 'Course Number 1';

    expect(span.textContent).toBe(TITLE.toUpperCase());
  });

  it('should call method onDeleteClick() after Delete clicked', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const btn: HTMLElement = nativeElement.querySelector('.course-delete');

    btn.dispatchEvent(new Event('click'));
    expect(component.onDeleteClick).toHaveBeenCalled();

  });

  it('should emit event "deleteRequest" with course id after Delete clicked', () => {

    component.deleteRequest.subscribe((courseId) => {
      expect(courseId).toBe(component.courseItem.id);
    });
  });

  it('should convert duration from number to "HHh MMmin" format', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const span: HTMLElement = nativeElement.querySelector('.course-duration');

    expect(span.textContent).toBe('4h 5min');
  });
});
