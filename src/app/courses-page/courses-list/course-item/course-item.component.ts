import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { CoursesListItem } from '../../../core/models/courses-list-item.model';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseItemComponent implements OnInit {

  @Input() courseItem: CoursesListItem;
  @Output() deleteRequest = new EventEmitter<CoursesListItem>();

  constructor() { }

  ngOnInit() { }

  onDeleteClick() {
    this.deleteRequest.emit(this.courseItem);
  }
}
