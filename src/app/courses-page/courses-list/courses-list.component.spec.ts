import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoursesListComponent } from './courses-list.component';
import { CoursesService } from '../../core/services/courses/courses.service';
import { CourseItemComponent } from './course-item/course-item.component';
import { OrderByPipe } from '../../shared/pipes/orderby/orderby.pipe';
import { AlternateBorderDirective } from '../../shared/directives/alternateborder.directive';
import { DurationPipe } from '../../shared/pipes/duration/duration.pipe';

const COURSES_MOCK = [{
  'id': 1,
  'title': 'Course Number 1',
  'date': 'June, 26 2017',
  'duration': 245,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}, {
  'id': 2,
  'title': 'Course Number 2',
  'date': 'September, 16 2017',
  'duration': 106,
  'topRated': false,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}];

class MockCoursesService {
  getCoursesList = () => COURSES_MOCK;
}

describe('CoursesListComponent', () => {
  let component: CoursesListComponent;
  let fixture: ComponentFixture<CoursesListComponent>;
  let coursesService: CoursesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesListComponent, CourseItemComponent, OrderByPipe, AlternateBorderDirective, DurationPipe ],
      providers: [
        CoursesListComponent,
        { provide: CoursesService, useClass: MockCoursesService }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListComponent);
    component = fixture.componentInstance;
    coursesService = TestBed.get(CoursesService);
    component.coursesItems = coursesService.getCoursesList();
    fixture.detectChanges();
    spyOn(component, 'loadMore');
    spyOn(component, 'removeCourse');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide courses list after Angular calls ngOnInit', () => {
    component.ngOnInit();
    expect(component.coursesItems).toEqual(coursesService.getCoursesList());
  });

  it('should call "loadMore" method after "Load More" btn clicked', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const btn: HTMLElement = nativeElement.querySelector('.load-more');

    btn.dispatchEvent(new Event('click'));
    expect(component.loadMore).toHaveBeenCalled();
  });

  it('should call "removeCourse" method after "Delete" course btn clicked', () => {
    const removeBtn = fixture.nativeElement.querySelector('.course-delete');

    removeBtn.dispatchEvent(new Event('click'));
    expect(component.removeCourse).toHaveBeenCalled();
  });

  it('should emit event "deleteRequest" with course id after "Delete" btn clicked', () => {
    const childInstance = TestBed.createComponent(CourseItemComponent).componentInstance;
    component.deleteRequest.subscribe((courseId) => {
      expect(courseId).toBe(childInstance.courseItem.id);
    });
  });
});
