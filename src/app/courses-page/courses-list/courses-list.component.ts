import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CoursesListItem } from '../../core/models/courses-list-item.model';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent implements OnInit {

  @Input() coursesItems: CoursesListItem[];
  @Output() deleteRequest = new EventEmitter<CoursesListItem>();
  @Output() editRequest = new EventEmitter<CoursesListItem>();

  constructor() { }

  ngOnInit(): void {}

  removeCourse(course): void {
    this.deleteRequest.emit(course);
  }

  editCourse(course): void {
    this.editRequest.emit(course);
  }

  loadMore(): void {
    console.log('Load more courses!!!!');
  }

}
