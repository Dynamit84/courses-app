import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesListComponent } from './courses-list.component';
import { CourseItemComponent } from './course-item/course-item.component';
import { SharedModule } from '../../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
  ],
  declarations: [
    CoursesListComponent,
    CourseItemComponent,
  ],
  exports: [
    CoursesListComponent
  ]
})
export class CoursesListModule { }
