import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoursesPageComponent } from './courses-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CourseItemComponent } from './courses-list/course-item/course-item.component';
import { CoursesListComponent } from './courses-list/courses-list.component';
import { OrderByPipe } from '../shared/pipes/orderby/orderby.pipe';
import { DurationPipe } from '../shared/pipes/duration/duration.pipe';
import { AlternateBorderDirective } from '../shared/directives/alternateborder.directive';
import { FilterByNamePipe } from '../shared/pipes/filter/filter-by-name.pipe';
import { MatDialogModule, MatButtonModule } from '@angular/material';
import { NotificationModalComponent } from '../shared/notification-modal/notification-modal.component';
import { COURSE_MOCK } from './courses-list/course-item/course-item.component.spec';

describe('CoursesPageComponent', () => {
  let component: CoursesPageComponent;
  let fixture: ComponentFixture<CoursesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, MatButtonModule],
      declarations: [
        CoursesPageComponent,
        CoursesListComponent,
        CourseItemComponent,
        NotificationModalComponent,
        OrderByPipe,
        DurationPipe,
        AlternateBorderDirective,
        FilterByNamePipe,
      ],
      providers: [
        FilterByNamePipe,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesPageComponent);
    component = fixture.componentInstance;
    spyOn(component, 'removeCourse');
    spyOn(component, 'toggleAddEditMode');
    spyOn(component.editCourse, 'emit');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call "removeCourse" method after "Delete" course btn clicked', () => {
    const removeBtn = fixture.nativeElement.querySelector('.course-delete');

    removeBtn.dispatchEvent(new Event('click'));
    expect(component.removeCourse).toHaveBeenCalled();
  });

  it('should call "toggleAddEditMode" method after "Edit" btn clicked', () => {
    const editBtn = fixture.nativeElement.querySelector('.course-edit');

    editBtn.dispatchEvent(new Event('click'));
    expect(component.toggleAddEditMode).toHaveBeenCalled();
  });

});
