import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { CoursesService } from '../core/services/courses/courses.service';
import { CoursesListItem } from '../core/models/courses-list-item.model';
import { FilterByNamePipe } from '../shared/pipes/filter/filter-by-name.pipe';
import {MatDialog} from '@angular/material';
import { NotificationModalComponent } from '../shared/notification-modal/notification-modal.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-courses-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPageComponent implements OnInit, OnDestroy {
  coursesItems: CoursesListItem[] = [];
  private coursesSubscription: Subscription;

  @Output() addCourse = new EventEmitter<any>();
  @Output() editCourse = new EventEmitter<CoursesListItem>();

  constructor(
    private coursesService: CoursesService,
    private filterByName: FilterByNamePipe,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.coursesSubscription = this.coursesService.getCoursesList().subscribe(courses => this.coursesItems = courses);
  }

  removeCourse(course): void {
    const { id, title } = course;
    const dialogRef = this.dialog.open(NotificationModalComponent, {
      height: '210px',
      width: '600px',
      data: {
        courseName: title
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.coursesService.removeCourse(id).subscribe(() => {
          this.coursesSubscription = this.coursesService.getCoursesList().subscribe(courses => this.coursesItems = courses);
        });
      }
    });
  }

  filter(filterValue: string): void {
    // this.coursesItems = this.filterByName.transform(this.coursesItems, filterValue);
    this.coursesService.filterCourses(filterValue).subscribe(result => this.coursesItems = result);
  }

  toggleAddEditMode(course?: CoursesListItem): void {
    if (course) {
      this.editCourse.emit(course);
      return;
    }
    this.addCourse.emit();
  }

  ngOnDestroy(): void {
    this.coursesSubscription.unsubscribe();
  }

}
