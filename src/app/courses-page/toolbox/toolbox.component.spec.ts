import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ToolboxComponent } from './toolbox.component';

describe('ToolboxComponent', () => {
  let component: ToolboxComponent;
  let fixture: ComponentFixture<ToolboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:      [ FormsModule ],
      declarations: [ ToolboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(component, 'searchCourse');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call method searchCourse after button click', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const btn: HTMLElement = nativeElement.querySelector('.btn-primary');

    btn.dispatchEvent(new Event('click'));
    expect(component.searchCourse).toHaveBeenCalled();
  });

});
