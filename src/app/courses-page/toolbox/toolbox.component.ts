import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.css']
})
export class ToolboxComponent implements OnInit {
  searchValue = '';

  @Output() filterCourses = new EventEmitter<string>();

  constructor() { }

  ngOnInit() { }

  searchCourse() {
    this.filterCourses.emit(this.searchValue);
  }

}
