import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { DurationComponent } from './duration/duration.component';
import { SharedModule } from '../shared/shared.module';
import { AddCoursePageComponent } from './add-course-page.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
  ],
  declarations: [DatePickerComponent, DurationComponent, AddCoursePageComponent],
  exports: [AddCoursePageComponent],
})
export class AddCoursePageModule { }
