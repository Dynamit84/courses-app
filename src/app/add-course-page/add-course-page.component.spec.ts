import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCoursePageComponent } from './add-course-page.component';
import { SharedModule } from '../shared/shared.module';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { DurationComponent } from './duration/duration.component';
import { CoursesListItem } from '../core/models/courses-list-item.model';

const EMPTY_COURSE_MOCK: CoursesListItem = {
  id: 0,
  title: '',
  description: '',
  date: '',
  topRated: false,
  duration: 0,
};

describe('AddCoursePageComponent', () => {
  let component: AddCoursePageComponent;
  let fixture: ComponentFixture<AddCoursePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCoursePageComponent, DatePickerComponent, DurationComponent ],
      imports: [ SharedModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCoursePageComponent);
    component = fixture.componentInstance;
    component.course = EMPTY_COURSE_MOCK;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
