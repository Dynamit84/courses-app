import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CoursesListItem } from '../core/models/courses-list-item.model';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from '../core/services/courses/courses.service';
import { Subscription } from 'rxjs';

const EMPTY_COURSE_MOCK = {
  id: 0,
  name: '',
  description: '',
  date: '',
  isTopRated: false,
  length: 0,
};

@Component({
  selector: 'app-add-course-page',
  templateUrl: './add-course-page.component.html',
  styleUrls: ['./add-course-page.component.css']
})
export class AddCoursePageComponent implements OnInit, OnDestroy {

  course: CoursesListItem;
  private courseSubscription: Subscription;

  @Output() cancelRequest = new EventEmitter<any>();

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getCourse();
  }

  getCourse(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.courseSubscription = this.coursesService.getCourseById(id).subscribe(course => this.course = course);
    } else {
      this.course = EMPTY_COURSE_MOCK;
    }
  }

  onSaveClick(): void {
    this.course.id = Date.now();
    this.course.date = String(new Date(Date.now()));
    this.coursesService.createCourse(this.course).subscribe(result => this.router.navigate(['courses']));
  }

  ngOnDestroy(): void {
    // this.courseSubscription.unsubscribe();
  }

}
