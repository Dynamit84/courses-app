import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-duration',
  templateUrl: './duration.component.html',
  styleUrls: ['./duration.component.css']
})
export class DurationComponent implements OnInit {

  @Input() duration: string;
  @Output() durationChange = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onDurationChange(value: string) {
    this.durationChange.emit(value);
  }

}
