import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {

  @Input() date: string;
  @Output() dateChange = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onDateChange(value: string) {
    this.dateChange.emit(value);
  }

}
