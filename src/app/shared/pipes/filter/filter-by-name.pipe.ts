import { Pipe, PipeTransform } from '@angular/core';
import { CoursesListItem } from '../../../core/models/courses-list-item.model';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {

  transform(courses: CoursesListItem[], filterValue: string): CoursesListItem[] {
    return courses.filter( course => course.name.includes(filterValue));
  }

}
