import { FilterByNamePipe } from './filter-by-name.pipe';

const COURSE_MOCK = [{
  'id': 1,
  'title': 'Angular5 Course',
  'date': 'June 29, 2017',
  'duration': 245,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}];

const COURSES_MOCK = [{
  'id': 1,
  'title': 'Angular5 Course',
  'date': 'June 29, 2017',
  'duration': 245,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}, {
  'id': 2,
  'title': 'React Course',
  'date': 'September 15, 2017',
  'duration': 106,
  'topRated': false,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}, {
  'id': 3,
  'title': 'NodeJs Course',
  'date': 'July 05, 2018',
  'duration': 120,
  'topRated': false,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}, {
  'id': 4,
  'title': 'HTML5 Course',
  'date': 'January 27, 2018',
  'duration': 45,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
}];

describe('FilterByNamePipe', () => {
  const pipe = new FilterByNamePipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms COURSES_MOCK with input value "Angular5" to COURSE_MOCK', () => {
    expect(pipe.transform(COURSES_MOCK, 'Angular5')).toEqual(COURSE_MOCK);
  });

  it('length of transformed array with input value "Course" should be 4', () => {
    expect(pipe.transform(COURSES_MOCK, 'Course').length).toBe(4);
  });

  it('transforms COURSES_MOCK with input value "qwerty" to []', () => {
    expect(pipe.transform(COURSES_MOCK, 'qwerty')).toEqual([]);
  });

});
