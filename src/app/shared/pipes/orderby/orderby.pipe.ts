import { Pipe, PipeTransform } from '@angular/core';
import { CoursesListItem } from '../../../core/models/courses-list-item.model';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(courses: CoursesListItem[]): CoursesListItem[] {
    return courses.sort( (course, nextCourse) => Date.parse(course.date) - Date.parse(nextCourse.date));
  }

}
