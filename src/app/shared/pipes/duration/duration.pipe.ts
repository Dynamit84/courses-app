import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(duration: number): string {
    if (duration < 60) {
      return `${duration}min`;
    }
    const hours = Math.floor(duration / 60);
    const minutes = duration - hours * 60;
    if (!minutes) {
      return `${hours}h`;
    }
    return `${hours}h ${(minutes > 0) && minutes}min`;
  }
}
