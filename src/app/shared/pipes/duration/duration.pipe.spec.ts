import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  const pipe = new DurationPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms 50 to "50min"', () => {
    expect(pipe.transform(50)).toBe('50min');
  });

  it('transforms 120 to "2h"', () => {
    expect(pipe.transform(120)).toBe('2h');
  });

  it('transforms 138 to "2h 18min"', () => {
    expect(pipe.transform(138)).toBe('2h 18min');
  });
});
