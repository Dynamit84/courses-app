import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.isAuthenticated = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have web app name', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const a: HTMLElement = nativeElement.querySelector('.navbar-brand');

    expect(a.textContent).toBe('Let\'s study!');
  });

  it('should have hardcoded user name when logged in', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const span: HTMLElement = nativeElement.querySelector('.user-name');

    expect(span.textContent).toBe('Login_Name');
  });

  it('shouldn\'t have hardcoded user name and "Log out" btn when logged out', () => {
    component.isAuthenticated = false;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.nativeElement;
    const actionsDiv: HTMLElement = nativeElement.querySelector('.header-actions');

    expect(actionsDiv).toBe(null);
  });
});
