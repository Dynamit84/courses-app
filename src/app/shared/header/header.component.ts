import { Component, OnInit, Input } from '@angular/core';
import { AuthorizationService } from '../../core/services/authorization/authorization.service';
import { Router } from '@angular/router';
import {User} from '../../core/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() isAuthenticated: boolean;
  @Input() currentUser: User;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
  ) { }

  ngOnInit() {}

  logout() {
    this.authorizationService.logout();
    this.router.navigate(['login']);
  }

}
