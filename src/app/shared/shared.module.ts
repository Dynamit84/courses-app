import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FakeLogoComponent } from './fake-logo/fake-logo.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { AlternateBorderDirective } from './directives/alternateborder.directive';
import { DurationPipe } from './pipes/duration/duration.pipe';
import { OrderByPipe } from './pipes/orderby/orderby.pipe';
import { FilterByNamePipe } from './pipes/filter/filter-by-name.pipe';
import { IconComponent } from './icon/icon.component';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { NotificationModalComponent } from './notification-modal/notification-modal.component';
import { MatDialogModule, MatButtonModule, MatInputModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    FakeLogoComponent,
    BreadcrumbComponent,
    AlternateBorderDirective,
    DurationPipe,
    OrderByPipe,
    FilterByNamePipe,
    IconComponent,
    NotificationModalComponent,
  ],
  entryComponents: [NotificationModalComponent],
  exports: [
    HeaderComponent,
    FooterComponent,
    FakeLogoComponent,
    BreadcrumbComponent,
    AlternateBorderDirective,
    DurationPipe,
    OrderByPipe,
    FilterByNamePipe,
    IconComponent,
    NotificationModalComponent,
    MatInputModule,
    MatButtonModule,
    FormsModule,
  ],
  providers: [
    FilterByNamePipe
  ]
})
export class SharedModule { }
