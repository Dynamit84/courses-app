import { Directive, ElementRef, Input, OnInit } from '@angular/core';

enum AlternateBorderColors {
  Green = 'green',
  Blue = 'blue',
}

const DAY_IN_MS = 86400000;

@Directive({
  selector: '[appAlternateBorder]'
})
export class AlternateBorderDirective implements OnInit {
  @Input('appAlternateBorder') courseDate: string;

  constructor(private el: ElementRef) {}
  ngOnInit() {
    this.alternateBorderColor(this.courseDate);
  }

  private alternateBorderColor(date: string) {
    const currentDate = Date.now();
    const creationDate = Date.parse(date);

    if (creationDate > currentDate) {
      this.el.nativeElement.style.borderColor = AlternateBorderColors.Blue;
    } else if (creationDate >= currentDate - 14 * DAY_IN_MS) {
      this.el.nativeElement.style.borderColor = AlternateBorderColors.Green;
    }
  }

}
