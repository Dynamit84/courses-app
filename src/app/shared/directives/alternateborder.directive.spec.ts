import { AlternateBorderDirective } from './alternateborder.directive';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
  <div [appAlternateBorder]="'July, 20 2018'">Green border</div>
  <div [appAlternateBorder]="'June, 26 2017'" style="border-color: #d3d3d3">The Default (Gray)</div>
  <div [appAlternateBorder]="'August, 26 2019'">Blue border</div>`
})
class TestComponent { }

describe('AlternateBorderDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let des: DebugElement[];

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [ AlternateBorderDirective, TestComponent ]
    })
      .createComponent(TestComponent);

    fixture.detectChanges();

    des = fixture.debugElement.queryAll(By.directive(AlternateBorderDirective));

  });

  it('should change border color for 1st <div> to "green"', () => {
    const brColor = des[0].nativeElement.style.borderColor;
    expect(brColor).toBe('green');
  });

  it('shouldn\'t change border color for 2nd <div>', () => {
    const brColor = des[1].nativeElement.style.borderColor;
    expect(brColor).toBe('rgb(211, 211, 211)');
  });

  it('should change border color for 3rd <div> to "blue"', () => {
    const brColor = des[2].nativeElement.style.borderColor;
    expect(brColor).toBe('blue');
  });

});
