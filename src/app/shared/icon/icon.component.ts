import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit {
  iconPath: string;

  @Input() iconName: string;

  constructor() { }

  ngOnInit() {
    this.iconPath = `/assets/images/${this.iconName}.svg`;
  }

}
