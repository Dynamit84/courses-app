import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatButtonModule } from '@angular/material';
import { NotificationModalComponent } from './notification-modal.component';
import { MAT_DIALOG_DATA } from '@angular/material';

describe('NotificationModalComponent', () => {
  let component: NotificationModalComponent;
  let fixture: ComponentFixture<NotificationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, MatButtonModule],
      declarations: [ NotificationModalComponent ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
