import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CoursesService } from '../../core/services/courses/courses.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  isEditPage = false;
  courseTitle: string;
  courseSubscription: Subscription;

  constructor(
    private router: Router,
    private coursesService: CoursesService,
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.courseTitle = '';
        const { url } = val;
        if (url.match(/\/courses\/\d+/)) {
          this.isEditPage = true;
          const id = Number(url.match(/\d+/)[0]);
          this.courseSubscription = this.coursesService.getCourseById(id).subscribe(course => this.courseTitle = course.name);
        } else {
          this.isEditPage = false;
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.isEditPage) {
      this.courseSubscription.unsubscribe();
    }
  }

}
