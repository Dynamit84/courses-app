import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../core/services/authorization/authorization.service';
import { Router } from '@angular/router';
import { User } from '../core/models/user.model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  model: Partial<User> = {
    userName: 'Warner',
    password: 'ea',
  };
  error = '';

  constructor(private authorizationService: AuthorizationService, private router: Router) { }

  ngOnInit() {}

  login() {
    this.authorizationService.login(this.model.userName, this.model.password).subscribe(() => {
      this.router.navigate(['courses']);
    });
  }

}
