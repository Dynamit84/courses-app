export interface CoursesListItem {
  id: number;
  name: string;
  description: string;
  date: string;
  isTopRated: boolean;
  length: number;
}
