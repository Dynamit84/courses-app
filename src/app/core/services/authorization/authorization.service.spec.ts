import { TestBed, inject } from '@angular/core/testing';

import { AuthorizationService } from './authorization.service';

let sut: AuthorizationService;

describe('AuthorizationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizationService]
    });
    sut = new AuthorizationService();
  });

  afterEach(() => {
    localStorage.removeItem('user');
    sut = null;
  });

  it('should be created', inject([AuthorizationService], (service: AuthorizationService) => {
    expect(service).toBeTruthy();
  }));

  it('should return "true" when user is authenticated', () => {
    localStorage['user'] = JSON.stringify({ email: '111@epam.com', token: '12345'});
    expect(sut.isAuthenticated()).toBeTruthy();
  });

  it('should return "false" when user isn\'t authenticated', () => {
    expect(sut.isAuthenticated()).toBeFalsy();
  });

  it('should add localStorage item after login', () => {
    sut.login('123@epam.com');
    expect(localStorage['user']).toBeTruthy();
  });

  it('should remove localStorage item after logout', () => {
    sut.logout();
    expect(localStorage['user']).toBeFalsy();
  });

  it('should return user info', () => {
    sut.login('123@epam.com');
    expect(sut.getUserInfo().email).toBe('123@epam.com');
  });
});
