import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

const LOGIN_URL = 'http://localhost:3004/auth/login';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  subject = new BehaviorSubject(false);

  constructor(private http: HttpClient) { }

  login(userName: string, password: string): Observable<any> {

    return this.http.post<any>(LOGIN_URL, {login: userName, password }).pipe(
      switchMap(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage['user'] =  JSON.stringify(user);
          this.subject.next(true);
        }
        return of(user);
      })
    );
  }

  logout(): void {
    localStorage.removeItem('user');
    this.subject.next(false);
  }

  getUserInfo(): User {

    return JSON.parse(localStorage['user']);
  }

  isAuthenticated(): Observable<boolean> {
    const currentUser = JSON.parse(localStorage.getItem('user'));
    if (currentUser && currentUser.token) {
      this.subject.next(true);
    }

    return this.subject.asObservable();
  }
}
