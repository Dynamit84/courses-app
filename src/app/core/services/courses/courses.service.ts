import { Injectable } from '@angular/core';
import { CoursesListItem } from '../../models/courses-list-item.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const COURSES_URL = 'http://localhost:3004/courses';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  courses: CoursesListItem[];

  constructor(
    private http: HttpClient,
  ) { }

  getCoursesList (): Observable<CoursesListItem[]> {

    return this.http.get<CoursesListItem[]>(COURSES_URL);
  }

  createCourse(newCourse: CoursesListItem): Observable<{}> {
    return this.http.post<CoursesListItem>(COURSES_URL, newCourse);
  }

  updateCourse(updatedCourse): CoursesListItem[] {
    return this.courses.map(course => {
      if (course.id === updatedCourse.id) {

        return updatedCourse;
      }

      return course;
    });
  }

  removeCourse(courseId: number): Observable<{}> {
    const courseUrl = `${COURSES_URL}/${courseId}`;

    return this.http.delete<CoursesListItem[]>(courseUrl);
  }

  getCourseById(courseId: number): Observable<CoursesListItem> {
    const courseUrl = `${COURSES_URL}/${courseId}`;

    return this.http.get<CoursesListItem>(courseUrl);
  }

  filterCourses(value: string): Observable<CoursesListItem[]> {
    const requestUrl = `${COURSES_URL}?textFragment=${value}`;

    return this.http.get<CoursesListItem[]>(requestUrl);
  }
}
