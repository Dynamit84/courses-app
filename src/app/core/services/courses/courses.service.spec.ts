import { TestBed, inject } from '@angular/core/testing';

import { CoursesService } from './courses.service';
import { afterEach } from 'selenium-webdriver/testing';

let sut: CoursesService;

const COURSE_MOCK = {
  'id': 11,
  'title': 'Angular5 Course',
  'date': 'June 29, 2017',
  'duration': 245,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
};
const COURSE_MOCK2 = {
  'id': 2,
  'title': 'Angular5 Course',
  'date': 'June 29, 2017',
  'duration': 150,
  'topRated': true,
  'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
  'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
  'sit amet.'
};

const COURSES_MOCK = [
  {
    'id': 1,
    'title': 'Course Number 1',
    'date': 'June 29, 2017',
    'duration': 245,
    'topRated': true,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 2,
    'title': 'Course Number 2',
    'date': 'September 15, 2017',
    'duration': 106,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 3,
    'title': 'Course Number 3',
    'date': 'July 05, 2018',
    'duration': 120,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 4,
    'title': 'Course Number 4',
    'date': 'January 27, 2018',
    'duration': 45,
    'topRated': true,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 5,
    'title': 'Course Number 5',
    'date': 'March 28, 2018',
    'duration': 36,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 6,
    'title': 'Course Number 6',
    'date': 'July 08, 2017',
    'duration': 118,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 7,
    'title': 'Course Number 7',
    'date': 'November 12, 2018',
    'duration': 58,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 8,
    'title': 'Course Number 8',
    'date': 'September 15, 2017',
    'duration': 214,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 9,
    'title': 'Course Number 9',
    'date': 'September 13, 2018',
    'duration': 38,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }, {
    'id': 10,
    'title': 'Course Number 10',
    'date': 'July 13, 2017',
    'duration': 170,
    'topRated': false,
    'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas egestas mauris in metus cursus, ' +
    'id molestie orci iaculis. Pellentesque interdum scelerisque urna eget consequat. Phasellus in imperdiet elit, ' +
    'sit amet.'
  }
];

describe('CoursesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoursesService]
    });
    sut = new CoursesService();
  });

  /*afterEach(() => {
    sut = null;
  });*/

  it('should be created', inject([CoursesService], (service: CoursesService) => {
    expect(service).toBeTruthy();
  }));

  it('should create new course', () => {
    expect(sut.createCourse(COURSE_MOCK)).toEqual([...COURSES_MOCK, COURSE_MOCK]);
  });

  it('should return course by id', () => {
    expect(sut.getCourseById(1)).toEqual(COURSES_MOCK.filter(course => course.id === 1)[0]);
  });

  it('should remove course by id', () => {
    expect(sut.removeCourse(1)).toEqual(COURSES_MOCK.filter(course => course.id !== 1));
  });

  it('should update course', () => {
    expect(sut.updateCourse(COURSE_MOCK2)[1]).toEqual(COURSE_MOCK2);
  });
});
