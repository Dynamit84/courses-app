/*import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { User } from '../core/models/user.model';

const TEST_USER: User = {
  id: 1,
  userName: 'test',
  password: 'test',
  firstName: 'Test',
  lastName: 'User',
  email: 'test@gmail.com'
};

@Injectable({
  providedIn: 'root'
})
export class FakeBackendAuthInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { userName, password } = TEST_USER;
    const { url, body, method } = request;

    if (url.endsWith('/login') && method === 'POST') {
      if (body.userName === userName && body.password === password) {
        // if login details are valid return 200 OK with a fake jwt token
        return of(new HttpResponse({ status: 200, body: { token: 'fake-jwt-token', ...TEST_USER } }));
      } else {
        // else return 400 bad request
        return throwError('Username or password is incorrect');
      }
    }

    return next.handle(request);
  }
}*/
