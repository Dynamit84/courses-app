import { TestBed, inject } from '@angular/core/testing';

import { FakeBacendAuthService } from './fake-backend-auth';

describe('FakeBacendAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FakeBacendAuthService]
    });
  });

  it('should be created', inject([FakeBacendAuthService], (service: FakeBacendAuthService) => {
    expect(service).toBeTruthy();
  }));
});
