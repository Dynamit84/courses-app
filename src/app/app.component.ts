import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from './core/services/authorization/authorization.service';
import { User } from './core/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  isLoggedIn = false;
  currentUser: User;

  constructor(public authorizationService: AuthorizationService) {}

  ngOnInit() {
    this.authorizationService.isAuthenticated().subscribe((isLoggedIn) => {
      this.isLoggedIn = isLoggedIn;
      if (this.isLoggedIn) {
        this.currentUser = this.authorizationService.getUserInfo();
      }
    });
  }
}
